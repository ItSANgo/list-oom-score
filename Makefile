#! /usr/bin/make -f

prefix=$(HOME)
bin_dir=$(prefix)/bin
TARGET=list_oom_score

.PHONY: all clean install uninstall
all:
clean:
install:
	install $(TARGET) $(bin_dir)
uninstall:
	cd $(bin_dir) && $(RM) $(TARGET)
